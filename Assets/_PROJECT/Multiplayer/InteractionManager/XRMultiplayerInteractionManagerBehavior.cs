﻿using FishNet.Connection;
using FishNet.Object;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace _PROJECT.Multiplayer
{
    public class XRMultiplayerInteractionManagerBehavior : NetworkBehaviour
    {
        private XRMultiplayerInteractionManager _manager;
        
        private void Awake()
        {
            _manager = GetComponentInParent<XRMultiplayerInteractionManager>();
        }
        
        [ServerRpc(RequireOwnership = false)]
        public void RequestSelectEnterBroadcastRPC(NetworkObject interactor, NetworkObject interactable, NetworkConnection conn = null)
        {
            Debug.Log("[SERVER] Received SelectEnter for " + interactable.name + " to " + interactor.name);
            
            // Get interactable and interactor.
            IXRSelectInteractable iInteractable = interactable.transform.GetComponent<IXRSelectInteractable>();
            IXRSelectInteractor iInteractor = interactor.transform.GetComponent<IXRSelectInteractor>();
            
            // If interactable or interactor is null, return.
            if (iInteractable == null || iInteractor == null)
            {
                Debug.LogWarning("[SERVER] Unexpected null interactable or interactor in RequestSelectEnterBroadcastRPC.");
                return;
            };
            
            // Broadcast select enter to all clients.
            _manager.BaseSelectEnter(iInteractor, iInteractable);
            SelectEnterBroadcastRPC(interactor, interactable);
        }
        
        [ObserversRpc]
        public void SelectEnterBroadcastRPC(NetworkObject interactor, NetworkObject interactable)
        {
            Debug.Log("[CLIENT] Recieved SelectEnter from server for " + interactable.name + " to " + interactor.name);
            
            // Get interactable and interactor.
            IXRSelectInteractable iInteractable = interactable.transform.GetComponent<IXRSelectInteractable>();
            IXRSelectInteractor iInteractor = interactor.transform.GetComponent<IXRSelectInteractor>();
            
            // If interactable or interactor is null, return.
            if (iInteractable == null || iInteractor == null)
            {
                Debug.LogWarning("[CLIENT] Unexpected null interactable or interactor in SelectEnterBroadcastRPC.");
                return;
            };
            
            _manager.BaseSelectEnter(iInteractor, iInteractable);
        }
        
        [ServerRpc(RequireOwnership = false)]
        public void RequestSelectExitBroadcastRPC(NetworkObject nInteractor, NetworkObject nInteractable)
        {
            Debug.Log("[SERVER] Received SelectExit for " + nInteractable.name + " to " + nInteractor.name);
            
            // Get interactable and interactor.
            IXRSelectInteractable iInteractable = nInteractable.transform.GetComponent<IXRSelectInteractable>();
            IXRSelectInteractor iInteractor = nInteractor.transform.GetComponent<IXRSelectInteractor>();
            
            // If interactable or interactor is null, return.
            if (iInteractable == null || iInteractor == null)
            {
                Debug.LogWarning("[SERVER] Unexpected null interactable or interactor in RequestSelectExitBroadcastRPC.");
                return;
            };
            
            // Broadcast select exit to all clients.
            _manager.BaseSelectExit(iInteractor, iInteractable);
        }
        
        [ObserversRpc]
        public void SelectExitBroadcastRPC(NetworkObject nInteractor, NetworkObject nInteractable)
        {
            Debug.Log("[CLIENT] Recieved SelectExit from server for " + nInteractable.name + " to " + nInteractor.name);
            
            // Get interactable and interactor.
            IXRSelectInteractable iInteractable = nInteractable.transform.GetComponent<IXRSelectInteractable>();
            IXRSelectInteractor iInteractor = nInteractor.transform.GetComponent<IXRSelectInteractor>();
            
            // If interactable or interactor is null, return.
            if (iInteractable == null || iInteractor == null)
            {
                Debug.LogWarning("[CLIENT] Unexpected null interactable or interactor in SelectExitBroadcastRPC.");
                return;
            };
            
            _manager.BaseSelectExit(iInteractor, iInteractable);
        }

    }
}