﻿using System;
using FishNet.Object;
using FishNet.Object.Synchronizing;
using UnityEngine;

namespace _PROJECT.Multiplayer
{
    public class ParentSync : NetworkBehaviour
    {
        [SyncVar] public NetworkObject parentNetworkObject;
        public override void OnStartClient()
        {
            base.OnStartClient();
            transform.parent = parentNetworkObject == null ? null : parentNetworkObject.transform;
        }

        private void Awake()
        {
            // Throw error if parent does not have a network object
            if (transform.parent != null && transform.parent.GetComponent<NetworkObject>() == null)
            {
                throw new Exception("ParentSync must be on a child of a network object.");
            }
            parentNetworkObject = transform.parent == null ? null : transform.parent.GetComponent<NetworkObject>();
        }

        private void OnTransformParentChanged()
        {
            if (!IsOwner || !IsServer) return;

            // Get new parent's network object.
            NetworkObject newParent = transform.parent == null ? null : transform.parent.GetComponent<NetworkObject>();

            if (newParent == null)
            {
                Debug.LogWarning("ParentSync: New parent is not a network object.");
                return;
            }

            SetParentRPC(GetComponent<NetworkObject>(),
                newParent);
        }

        [ServerRpc(RequireOwnership = false)]
        private void SetParentRPC(NetworkObject obj, NetworkObject newParent)
        {
            transform.parent = newParent == null ? null : newParent.transform;
            parentNetworkObject = newParent;
            SetParentBroadcastRPC(obj, newParent);
        }

        [ObserversRpc]
        private void SetParentBroadcastRPC(NetworkObject obj, NetworkObject newParent)
        {
            transform.parent = newParent == null ? null : newParent.transform;
        }
    }
}