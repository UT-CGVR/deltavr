using System;
using System.Collections;
using System.Collections.Generic;
using _PROJECT;
using _PROJECT.Multiplayer;
using FishNet.Object;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using UnityEngine.XR.Management;

public class NetworkPlayerMirror : NetworkBehaviour
{
    public GameObject vrPlayerPrefab;
    public GameObject kbmPlayerPrefab;
    private XRPlayerMirror _networkMirror;
    private XROwnershipRequester _ownershipRequester;

    public override void OnStartClient()
    {
        base.OnStartClient();
        _networkMirror = GetComponentInChildren<XRPlayerMirror>();
        _ownershipRequester = GetComponent<XROwnershipRequester>();
        if (!IsOwner)
        {
            _networkMirror.enabled = false;
            return;
        }
        
        bool useVR = PlayerPrefs.GetInt("UseVR", 0) == 1;

        if (useVR)
        {
            StartCoroutine(StartXR());
        }
        else
        {
            GameObject kbmPlayer = Instantiate(kbmPlayerPrefab, transform.position, transform.rotation);
            _networkMirror.kbmPlayerTransform = kbmPlayer.GetComponentInChildren<Camera>().transform;
        }
    }

    public IEnumerator StartXR()
    {
        StopVR();

        Debug.Log("Initializing XR...");
        yield return XRGeneralSettings.Instance.Manager.InitializeLoader();

        while (!XRGeneralSettings.Instance.Manager.isInitializationComplete)
        {
            yield return new WaitForSeconds(0.1f);
        }

        if (XRGeneralSettings.Instance.Manager.activeLoader == null)
        {
            Debug.LogError("Initializing XR Failed. Check Editor or Player log for details.");
        }
        else
        {
            Debug.Log("Starting XR...");
            XRGeneralSettings.Instance.Manager.StartSubsystems();
        }

        GameObject vrPlayer = Instantiate(vrPlayerPrefab, transform.position, transform.rotation);
        _networkMirror.headTransform = vrPlayer.transform.FindRecursive("Main Camera");
        
        GameObject leftHand = vrPlayer.transform.FindRecursive("Left Hand").gameObject;
        GameObject rightHand = vrPlayer.transform.FindRecursive("Right Hand").gameObject;
        
        _networkMirror.leftHandTransform = leftHand.transform;
        _networkMirror.rightHandTransform = rightHand.transform;
        
        XRDirectInteractor leftInteractor = leftHand.GetComponent<XRDirectInteractor>();
        XRDirectInteractor rightInteractor = rightHand.GetComponent<XRDirectInteractor>();
        
        leftInteractor.selectEntered.AddListener(_ownershipRequester.OnSelectEnter);
        leftInteractor.selectExited.AddListener(_ownershipRequester.OnSelectExit);
        
        rightInteractor.selectEntered.AddListener(_ownershipRequester.OnSelectEnter);
        rightInteractor.selectExited.AddListener(_ownershipRequester.OnSelectExit);
        
        Debug.Log("XR mirror init done.");
    }
    
    
    public void OnDestroy()
    {
        if (!IsOwner) return;
        StopVR();
    }

    private void StopVR()
    {
        if (!IsOwner) return;

        Debug.Log("Stopping XR...");
        XRGeneralSettings.Instance.Manager.StopSubsystems();
        XRGeneralSettings.Instance.Manager.DeinitializeLoader();
        Debug.Log("XR stopped completely.");
    }

    public void OnApplicationQuit()
    {
        if (!IsOwner) return;
        StopVR();
    }
}