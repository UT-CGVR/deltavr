using System.Collections.Generic;
using FishNet.Component.Transforming;
using FishNet.Connection;
using FishNet.Managing;
using FishNet.Object;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace _PROJECT.Multiplayer
{
    public class XROwnershipRequester : NetworkBehaviour
    {

        [ServerRpc(RequireOwnership = false)]
        private void SendOwnershipRequest(NetworkObject networkObject, NetworkConnection conn = null)
        {
            NetworkTransform networkTransform = networkObject.GetComponent<NetworkTransform>();
            if (conn == null) return;
            if (networkTransform != null) networkTransform.GiveOwnership(conn);
            networkObject.GiveOwnership(conn);
        }
        
        [ServerRpc(RequireOwnership = false)]
        private void RemoveOwnership(NetworkObject networkObject, NetworkConnection conn = null)
        {
            NetworkTransform networkTransform = networkObject.GetComponent<NetworkTransform>();
            if (conn == null) return;
            if (networkTransform != null) networkTransform.RemoveOwnership();
            networkObject.RemoveOwnership();
        }

        
        public void OnSelectEnter(SelectEnterEventArgs eventArgs)
        {
            Debug.Log("Requesting ownership of " + eventArgs.interactableObject.transform.name);
            NetworkObject networkObject = eventArgs.interactableObject.transform.GetComponent<NetworkObject>();
            if (networkObject != null)
                SendOwnershipRequest(networkObject);
        }
        
        public void OnSelectExit(SelectExitEventArgs eventArgs)
        {
            // If the object is still selected, don't remove ownership. This is for multiselect support.
            if (eventArgs.interactableObject.isSelected) return;
            Debug.Log("Unrequesting ownership of " + eventArgs.interactableObject.transform.name);
            NetworkObject networkObject = eventArgs.interactableObject.transform.GetComponent<NetworkObject>();
            if (networkObject != null)
                RemoveOwnership(networkObject);
        }
    }
}