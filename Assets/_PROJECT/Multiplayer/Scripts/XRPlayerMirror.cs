using System.Collections;
using System.Collections.Generic;
using FishNet.Object;
using UnityEngine;
using UnityEngine.XR;

public class XRPlayerMirror : NetworkBehaviour
{
    public GameObject head;
    public GameObject leftHand;
    public GameObject rightHand;
    public bool useVR;

    public Transform headTransform;
    public Transform leftHandTransform;
    public Transform rightHandTransform;
    public Transform kbmPlayerTransform;
    
    public Vector3 leftHandPositionOffset;
    public Vector3 rightHandPositionOffset;
    
    public Vector3 leftHandRotationOffset;
    public Vector3 rightHandRotationOffset;
    
    private Animator _leftHandAnimator;
    private Animator _rightHandAnimator;

    private static readonly int Trigger = Animator.StringToHash("Trigger");
    private static readonly int Grip = Animator.StringToHash("Grip");

    public override void OnStartClient()
    {
        base.OnStartClient();
        if (!IsOwner) return;
        // Disable head and hand meshrenderers
        useVR = PlayerPrefs.GetInt("UseVR", 0) == 1;
            
        var meshRenderers = GetComponentsInChildren<MeshRenderer>();
        foreach (var mRenderer in meshRenderers)
        {
            mRenderer.enabled = false;
        }
            
        var skinnedMeshRenderers = GetComponentsInChildren<SkinnedMeshRenderer>();
        foreach (var mRenderer in skinnedMeshRenderers)
        {
            mRenderer.enabled = false;
        }
            
        _leftHandAnimator = leftHand.GetComponent<Animator>();
        _rightHandAnimator = rightHand.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!IsOwner) return;
        
        if (useVR)
        {
            UpdateXR();
        }
        else
        {
            head.transform.position = kbmPlayerTransform.position;
            head.transform.rotation = kbmPlayerTransform.rotation;
        }
    }

    private void UpdateXR()
    {
        if (headTransform != null)
        {
            head.transform.position = headTransform.position;
            head.transform.rotation = headTransform.rotation;
        }

        if (leftHandTransform != null)
        {
            leftHand.transform.position = leftHandTransform.position + leftHandPositionOffset;
            leftHand.transform.rotation = leftHandTransform.rotation * Quaternion.Euler(leftHandRotationOffset) ;
        }

        if (rightHandTransform != null)
        {
            rightHand.transform.position = rightHandTransform.position + rightHandPositionOffset;
            rightHand.transform.rotation = rightHandTransform.rotation * Quaternion.Euler(rightHandRotationOffset);
        }
        
        UpdateHandAnimation();
    }
    
    private void UpdateHandAnimation()
    {
        InputDevice leftHandDevice = InputDevices.GetDeviceAtXRNode(XRNode.LeftHand);
        InputDevice rightHandDevice = InputDevices.GetDeviceAtXRNode(XRNode.RightHand);

        _leftHandAnimator.SetFloat(Trigger, GetFeatureValueOrZero(CommonUsages.trigger, leftHandDevice));
        _leftHandAnimator.SetFloat(Grip, GetFeatureValueOrZero(CommonUsages.grip, leftHandDevice));
        
        _rightHandAnimator.SetFloat(Trigger, GetFeatureValueOrZero(CommonUsages.trigger, rightHandDevice));
        _rightHandAnimator.SetFloat(Grip, GetFeatureValueOrZero(CommonUsages.grip, rightHandDevice));

    }

    private float GetFeatureValueOrZero(InputFeatureUsage<float> feature, InputDevice targetDevice)
    {
        return targetDevice.TryGetFeatureValue(feature, out float value) ? value : 0;
    }

}