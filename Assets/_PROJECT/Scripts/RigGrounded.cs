using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RigGrounded : MonoBehaviour
{
    public LayerMask layermask;
    public Transform head;
    void Update()
    {
        RaycastHit hit;
        if(Physics.Raycast(head.position, Vector3.down, out hit, Mathf.Infinity, layermask))
        {
            Debug.Log(hit.transform.name);
            transform.position = new Vector3(transform.position.x, hit.point.y, transform.position.z);
        }
    }
}
