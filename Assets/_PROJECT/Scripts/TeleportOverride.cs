using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class TeleportOverride : MonoBehaviour
{
    private PlayerActions input;
    private Vector2 axis;
    private bool rayEnabled;
    public MonoBehaviour ray;

    private void Awake()
    {
        input = new PlayerActions();
        input.XRIRightHandLocomotion.TeleportModeActivate.performed += ctx => CallRay();
    }

    // Update is called once per frame
    void Update()
    {
    }

    void CallRay()
    {
        if (rayEnabled)
        {
            ray.enabled = false;
            rayEnabled = false;
        }
        else
        {
            ray.enabled = true;
            rayEnabled = true;
        }
    }

    private void OnEnable()
    {
        input.Enable();
    }

    private void OnDisable()
    {
        input.Disable();
    }
}