using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavPoint : MonoBehaviour
{
    public NavPoint previous;
    public NavPoint next;

    public NavPoint GetNext()
    {
        return next;
    }
    
    void OnDrawGizmos()
    {
        if (next == null) return;
        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(transform.position, next.transform.position);
    }

}
