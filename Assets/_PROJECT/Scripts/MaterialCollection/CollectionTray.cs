using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectionTray : MonoBehaviour
{
    public List<GameObject> row1;
    public List<GameObject> row2;
    public List<GameObject> row3;
    public List<GameObject> row4;


    public void RowShow(int i)
    {
        switch (i)
        {
            case 1:
                foreach(GameObject go in row1)
                {
                    go.SetActive(true);
                }
                break;
            case 2:
                foreach (GameObject go in row2)
                {
                    go.SetActive(true);
                }
                break;
            case 3:
                foreach (GameObject go in row3)
                {
                    go.SetActive(true);
                }
                break;
            case 4:
                foreach (GameObject go in row4)
                {
                    go.SetActive(true);
                }
                break;
        }
    }

}
