using System;

public static class Events
{
    public static event Action OnBreakoutEndGame;
    public static void BreakoutEndGame() => OnBreakoutEndGame?.Invoke();
    public static event Action OnBreakoutStartGame;
    public static void BreakoutStartGame() => OnBreakoutStartGame?.Invoke();
    
    public static event Action<float> OnBreakoutSetSpeed;
    public static void BreakoutSetSpeed(float speed) => OnBreakoutSetSpeed?.Invoke(speed);

    public static event Action OnBreakoutReduceBalls;
    public static void ReduceBalls() => OnBreakoutReduceBalls?.Invoke();
    
    public static event Action OnBreakoutAddBalls;
    public static void AddBalls() => OnBreakoutAddBalls?.Invoke();


}
