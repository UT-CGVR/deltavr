using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PhysButton : MonoBehaviour
{

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "MenuHand")
        {
            GetComponent<Button>().onClick.Invoke();
        }
    }
}
