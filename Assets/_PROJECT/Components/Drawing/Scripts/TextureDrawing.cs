using UnityEngine;

namespace _PROJECT.Components.Drawing
{
    public class TextureDrawing : MonoBehaviour
    {
        public Texture2D initialDrawingTexture;
        private MeshRenderer _meshRenderer;
        private Texture2D _tempTexture;
        public float maxBrushSize = 100f;

        public NetworkDrawingManager networkDrawingManager;

        void Start()
        {
            _meshRenderer = GetComponent<MeshRenderer>();
            networkDrawingManager = GetComponent<NetworkDrawingManager>();

            Vector3 scale = transform.lossyScale;

            _tempTexture = ResizeTexture(initialDrawingTexture, Mathf.RoundToInt(scale.x * initialDrawingTexture.width), Mathf.RoundToInt(scale.y * initialDrawingTexture.height));

            _meshRenderer.material.mainTexture = _tempTexture;
        }

        public void SetTempTexture(Texture2D texture)
        {
            _tempTexture = texture;
            _meshRenderer.material.mainTexture = _tempTexture;
        }

        public void Draw(Vector2 textureCoord, Color color, int size, Vector3 angle) {
            networkDrawingManager.RequestDrawOnTextureRPC(textureCoord, color, size, angle);
        }

        public void DrawOnTexture(Vector2 textureCoord, Color color, int size, Vector3 angle)
        {
            int x = Mathf.FloorToInt(textureCoord.x * _tempTexture.width);
            int y = Mathf.FloorToInt(textureCoord.y * _tempTexture.height);

            float sprayAngle = Mathf.Atan2(angle.y, angle.x) * Mathf.Rad2Deg;
            Quaternion rotation = Quaternion.AngleAxis(sprayAngle, Vector3.forward);
    
            float density = Mathf.Lerp(1.0f, 0.1f, size / maxBrushSize);

            float radius = size * density;
            int numPixels = Mathf.RoundToInt(Mathf.PI * radius * radius);
    
            for (int i = 0; i < numPixels; i++)
            {
                Vector2 offset = new Vector2(Random.Range(-size, size), Random.Range(-size, size));
                if (offset.magnitude > size) continue;
                offset = rotation * offset;

                float alpha = Random.Range(0f, 1f);
                Color sprayColor = new Color(color.r, color.g, color.b, alpha * color.a);

                _tempTexture.SetPixel(x + Mathf.RoundToInt(offset.x), y + Mathf.RoundToInt(offset.y), sprayColor);
            }

            _tempTexture.Apply();
        }
        
        private Texture2D ResizeTexture(Texture2D inputTexture, int newWidth, int newHeight)
        {
            Texture2D outputTexture = new Texture2D(newWidth, newHeight);
            Color[] pixels = new Color[newWidth * newHeight];

            // Resample the input texture to the new size
            for (int y = 0; y < newHeight; y++)
            {
                for (int x = 0; x < newWidth; x++)
                {
                    float u = x / (float)newWidth;
                    float v = y / (float)newHeight;

                    pixels[y * newWidth + x] = inputTexture.GetPixelBilinear(u, v);
                }
            }

            // Set the pixels of the output texture
            outputTexture.SetPixels(pixels);
            outputTexture.Apply();

            return outputTexture;
        }

    }
}