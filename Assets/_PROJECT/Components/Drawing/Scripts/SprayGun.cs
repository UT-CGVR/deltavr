using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace _PROJECT.Components.Drawing
{
    public class SprayGun : XRGrabInteractable
    {
        public GameObject trigger;
        public ParticleSystem sprayPart;
        public Transform sprayPoint;
        public float maxDistance = 0.5f;

        public Color sprayColor;
        public int spraySize = 100;

        private int _layerMask;

        private float _cooldownTimer = 0f;
        private bool _isSpraying;

        protected override void OnActivated(ActivateEventArgs args)
        {
            base.OnActivated(args);
            StartSpray();
        }

        protected override void OnDeactivated(DeactivateEventArgs args)
        {
            base.OnDeactivated(args);
            StopSpray();
        }

        private new void Awake()
        {
            base.Awake();
            sprayPart.Stop(true, ParticleSystemStopBehavior.StopEmitting);
            _isSpraying = false;
            _layerMask = 1 << LayerMask.NameToLayer("Paintable");
        }

        private void StartSpray()
        {
            _isSpraying = true;
            trigger.transform.Rotate(0f, 0f, -8.5f);
            sprayPart.Play(true);
        }

        private void StopSpray()
        {
            _isSpraying = false;
            trigger.transform.Rotate(0f, 0f, 8.5f);
            sprayPart.Stop(true, ParticleSystemStopBehavior.StopEmitting);
        }

        private void Update()
        {
            if (!isSelected || !_isSpraying)
            {
                if (!sprayPart.isPlaying) return;
                sprayPart.Stop(true, ParticleSystemStopBehavior.StopEmitting);
                _isSpraying = false;
                return;
            }

            if (_cooldownTimer > 0f)
            {
                _cooldownTimer -= Time.deltaTime;
                return;
            }

            RaycastHit hit;
            if (!Physics.Raycast(sprayPoint.position, sprayPoint.forward, out hit, maxDistance, _layerMask))
            {
                Debug.Log("No hit");
                Debug.DrawLine(sprayPoint.position, sprayPoint.position + sprayPoint.forward * maxDistance, Color.red);
                _cooldownTimer = 0.1f;
                return;
            }

            Debug.Log("Hit " + hit.collider.name);
            Debug.DrawLine(sprayPoint.position, hit.point, Color.green); // Draw a line in the scene view

            TextureDrawing textureDrawing = hit.collider.GetComponent<TextureDrawing>();
            if (textureDrawing == null) return;
            Vector2 textureCoord = hit.textureCoord;

            // Calculate the spray size based on the distance from the spray point to the hit point
            int adjustedSpraySize = Mathf.RoundToInt(Mathf.Lerp(0f, spraySize,
                Vector3.Distance(sprayPoint.position, hit.point) / maxDistance));
            _cooldownTimer = 0.05f;
            
            textureDrawing.Draw(textureCoord, sprayColor, adjustedSpraySize, sprayPoint.forward);
        }
    }
}