using FishNet.Connection;
using FishNet.Object;
using UnityEngine;

namespace _PROJECT.Components.Drawing
{
    public class NetworkDrawingManager : NetworkBehaviour
    {
        private TextureDrawing _textureDrawing;
    
        public override void OnStartServer()
        {
            base.OnStartServer();
            _textureDrawing = GetComponent<TextureDrawing>();
        }

        [ServerRpc(RequireOwnership = false)]
        public void RequestDrawOnTextureRPC(Vector2 textureCoord, Color drawColor, int brushSize, Vector3 angle)
        {
            DrawOnTextureRPC(textureCoord, drawColor, brushSize, angle);
        }

        public override void OnStartClient()
        {
            base.OnStartClient();
            _textureDrawing = GetComponent<TextureDrawing>();
        }
    
        [ServerRpc(RequireOwnership = false)]
        public void RequestCurrentTextureRPC(NetworkConnection conn = null)
        {
            if (_textureDrawing == null)
                return;
        
            SendCurrentTextureRPC(conn, _textureDrawing.initialDrawingTexture);
        }

        [TargetRpc]
        private void SendCurrentTextureRPC(NetworkConnection conn, Texture2D currentTexture)
        {
            if (_textureDrawing == null)
                return;
        
            _textureDrawing.SetTempTexture(currentTexture);
        }
    
        [ObserversRpc]
        private void DrawOnTextureRPC(Vector2 textureCoord, Color drawColor, int brushSize, Vector3 angle)
        {
            if (_textureDrawing == null)
                return;
        
            _textureDrawing.DrawOnTexture(textureCoord, drawColor, brushSize, angle);
        }
    }
}