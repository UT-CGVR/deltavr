using System;
using FishNet.Object;
using UnityEngine;

namespace _PROJECT.Components.Mole
{
    public class Mole : NetworkBehaviour
    {
        private bool _movingUp;
        private bool _movingDown;
        private bool _isUp;
        private bool _canBeHit;
        private float _timer;
        private float _distance;
        private float _appearTime;
        private float _upTime;
        private float _disappearTime;
        private WhackAMoleMachine _machine;
        
        private Vector3 _startPosition;

        public void Rise(float distance, float appearTime, float disappearTime, WhackAMoleMachine machine)
        {
            if (!IsServer) return;
            if (_movingUp || _movingDown || _isUp) return;
            _distance = distance;
            _appearTime = appearTime;
            _disappearTime = disappearTime;
            _machine = machine;
            _movingUp = true;
            _timer = 0f;
        }

        private void Start()
        {
            _startPosition = transform.position;
        }

        void Update()
        {
            if (!IsServer) return;
            _timer += Time.deltaTime;

            if (_movingUp)
            {
                _canBeHit = true;
                transform.Translate(Vector3.up * (Time.deltaTime * _distance / _appearTime));

                if (!(_timer >= _appearTime)) return;
                _movingUp = false;
                _isUp = true;
                _timer = 0f;
                return;
            }

            if (_isUp)
            {
                _canBeHit = true;
                if (!(_timer >= _upTime)) return;
                _movingDown = true;
                _isUp = false;
                _timer = 0f;
                return;
            }

            if (_movingDown)
            {
                _canBeHit = false;
                transform.Translate(Vector3.down * (Time.deltaTime * _distance / _disappearTime));

                if (!(_timer >= _disappearTime)) return;
                _movingDown = false;
                _timer = 0f;
            }
        }

        void OnTriggerEnter(Collider _collider)
        {
            if (!IsServer) return;
            Debug.Log("Mole Hit");
            if (!_canBeHit) return;
            if (!_collider.gameObject.CompareTag("Destroyer")) return;
            _machine.MoleHit();
            _canBeHit = false;
            _movingDown = false;
            _movingUp = false;
            _isUp = false;
            _timer = 0f;
            transform.position = _startPosition;
        }
    }
}