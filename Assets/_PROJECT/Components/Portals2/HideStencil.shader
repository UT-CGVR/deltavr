Shader "Unlit/HideStencil"
{
	Properties
	{
		[IntRange] _StencilID("Stencil ID", Range(1, 255)) = 1
	}
		
	SubShader
	{
		Tags
		{
			"RenderType" = "Opaque"
			"Queue" = "Geometry"
			"RenderPipeline" = "UniversalPipeline"
		}

		Pass
		{
			Stencil
			{
				Ref[_StencilID]
				Comp NotEqual
			}
		}
	}
}
