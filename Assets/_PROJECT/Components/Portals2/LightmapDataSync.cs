using UnityEngine;

namespace _PROJECT.Components.Portals2
{
    [ExecuteInEditMode]
    public class LightmapDataSync : MonoBehaviour
    {
        public MeshRenderer toCloneRenderer;

        void Start()
        {
            // Get Mesh renderer;
            MeshRenderer meshRenderer = GetComponent<MeshRenderer>();
            if (meshRenderer == null)
            {
                Debug.LogError("Mesh renderer not found");
                return;
            }
            // Set data
            meshRenderer.lightmapIndex = toCloneRenderer.lightmapIndex;
            meshRenderer.lightmapScaleOffset = toCloneRenderer.lightmapScaleOffset;
            //meshRenderer.receiveGI = receiveGI;
            //meshRenderer.stitchLightmapSeams = stitchLightmapSeams;
           // meshRenderer.scaleInLightmap = scaleInLightmap;
        }
    }
}
