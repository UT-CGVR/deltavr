Shader "SkyboxTest"
{
    Properties
    {
        _Cube ("Environment Map", Cube) = "" {}
        _RotationY ("RotationY", Float) = 0
        _RotationX ("RotationX", Float) = 0
        _Exposure ("Exposure", Float) = 1

    }
    SubShader
    {
        Tags
        {
            "Queue" = "Background"
        }

        Pass
        {
            ZWrite Off
            Cull Front

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            // User-specified uniforms
            uniform samplerCUBE _Cube;
            float _RotationY;
            float _RotationX;
            float _Exposure;

            struct vertexInput
            {
                float4 vertex : POSITION;

                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct vertexOutput
            {
                float4 pos : SV_POSITION;
                float3 viewDir : TEXCOORD1;

                UNITY_VERTEX_INPUT_INSTANCE_ID
                UNITY_VERTEX_OUTPUT_STEREO
            };

            vertexOutput vert(vertexInput input)
            {
                vertexOutput output;

                UNITY_SETUP_INSTANCE_ID(input);
                UNITY_INITIALIZE_OUTPUT(vertexOutput, output);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);


                float4x4 modelMatrix = unity_ObjectToWorld;
                output.viewDir = mul(modelMatrix, input.vertex).xyz
                    - _WorldSpaceCameraPos;

                // Add float _RotationY to the view direction
                output.viewDir = mul(output.viewDir,
                                     float3x3(1, 0, 0, 0, cos(radians(_RotationY)), sin(radians(_RotationY)), 0,
                                              -sin(radians(_RotationY)), cos(radians(_RotationY))));

                // Add float _RotationX to the view direction
                output.viewDir = mul(output.viewDir,
                                     float3x3(cos(radians(_RotationX)), 0, -sin(radians(_RotationX)), 0, 1, 0,
                                              sin(radians(_RotationX)), 0, cos(radians(_RotationX))));

                output.pos = UnityObjectToClipPos(input.vertex);
                return output;
            }

            float4 frag(vertexOutput input) : COLOR
            {
                // Flip _Cube horizontally
                input.viewDir.x = -input.viewDir.x;
                float4 cube = texCUBE(_Cube, input.viewDir).rgba;
                // Add float _Exposure to the cube
                cube *= _Exposure;

                return cube;
            }
            ENDCG
        }
    }
}