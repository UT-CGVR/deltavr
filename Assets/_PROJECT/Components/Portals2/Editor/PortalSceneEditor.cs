using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(PortalScene))]
public class PortalSceneEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if (GUILayout.Button("Clone target portal"))
        {
            // Call BuildPortalScene on monobehavior
            ((PortalScene) target).BuildPortalScene();
        }
    }
}