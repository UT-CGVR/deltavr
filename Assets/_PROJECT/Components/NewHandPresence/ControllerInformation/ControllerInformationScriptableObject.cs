using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ControllerInformationScriptableObject", menuName = "CustomXR/ControllerInformationScriptableObject", order = 1)]
public class ControllerInformationScriptableObject : ScriptableObject
{
    [Header("Info")]
    public string controllerName;

    [Header("Controller")]
    public GameObject controllerPrefab;
    public Vector3 controllerOffset;
    public Vector3 controllerRotationOffset;
    public float vibrationStrength = 0.5f;
    public float vibrationDuration = 0.5f;

    [Header("Hand")]
    public GameObject handPrefab;
    public Vector3 handOffset;
    public Vector3 handRotationOffset;
    
    [Header("Tutorial strings")]
    public string turnHintText;
    public string moveHintText;
    public string teleportStartHintText;
    public string invalidTeleportHintText;
    public string teleportHintText;
    public string gripHintText;
}
