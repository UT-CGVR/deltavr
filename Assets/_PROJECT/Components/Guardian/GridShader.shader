Shader "Custom/GridShader"
{
    Properties
    {
        _GridColor ("Grid Color", Color) = (1, 1, 1, 1)
        _GridThickness ("Grid Thickness", Range(0, 0.1)) = 0.02
        _GridSpacing ("Grid Spacing", Range(0, 1)) = 0.1
        _CutoffDistance ("Cutoff Distance", Range(0, 10)) = 1
    }
    SubShader
    {
        Tags
        {
            "Queue"="Transparent" "RenderType"="Transparent"
        }
        Pass
        {
            Blend SrcAlpha OneMinusSrcAlpha
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct v2f
            {
                float4 pos : SV_POSITION;
                float3 worldPos : TEXCOORD0;
                UNITY_VERTEX_OUTPUT_STEREO
            };

            float4 _GridColor;
            float _GridThickness;
            float _GridSpacing;
            float _CutoffDistance;

            v2f vert(appdata v)
            {
                v2f o;
                UNITY_SETUP_INSTANCE_ID(v);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
                o.pos = UnityObjectToClipPos(v.vertex);
                o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
                return o;
            }

            half4 frag(v2f i) : SV_Target
            {
                half4 gridColor = _GridColor;
                float3 worldPos = i.worldPos;
                float dist = distance(_WorldSpaceCameraPos, worldPos);
                const float gridDistance = fmod(dist, _GridSpacing);

                if (dist <= _CutoffDistance)
                {
                    if (gridDistance < _GridThickness || gridDistance > _GridSpacing - _GridThickness)
                    {
                        return gridColor;
                    }
                    return half4(gridColor.r, gridColor.g, gridColor.b, 0.05);
                }

                return half4(0, 0, 0, 0); // return fully transparent color
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
}