﻿using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace _PROJECT.Scripts.Bow
{
    [RequireComponent(typeof(ArrowSpawner))]
    public class Notch : XRSocketInteractor
    {
        [SerializeField, Range(0, 1)] private float releaseThreshold = 0.25f;

        public global::Bow Bow { get; private set; }
        public PullMeasurer PullMeasurer { get; private set; }
        
        private ArrowSpawner _arrowSpawner;

        protected override void Awake()
        {
            base.Awake();
            Bow = GetComponentInParent<global::Bow>();
            _arrowSpawner = GetComponent<ArrowSpawner>();
            PullMeasurer = GetComponentInChildren<PullMeasurer>();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            PullMeasurer.selectExited.AddListener(ReleaseArrow);
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            PullMeasurer.selectExited.RemoveListener(ReleaseArrow);
        }

        public void ReleaseArrow(SelectExitEventArgs args)
        {
            Debug.Log("Requesting arrow launch...");
            _arrowSpawner.LaunchArrow();
        }

        public override void ProcessInteractor(XRInteractionUpdateOrder.UpdatePhase updatePhase)
        {
            base.ProcessInteractor(updatePhase);

            // Move attach when bow is pulled, this updates the renderer as well
            attachTransform.position = PullMeasurer.PullPosition;
        }
        
        public override bool CanSelect(IXRSelectInteractable interactable)
        {
            return QuickSelect(interactable) && CanHover(interactable) && interactable is Arrow;
        }
    
        private bool QuickSelect(IXRSelectInteractable interactable)
        {
            // This lets the Notch automatically grab the arrow
            return !hasSelection || IsSelecting(interactable);
        }

        private bool CanHover(IXRSelectInteractable interactable)
        {
            if (interactable is IXRHoverInteractable hoverInteractable)
                return CanHover(hoverInteractable);

            return false;
        } 
    }
}
