﻿using UnityEngine;

namespace _PROJECT.Scripts.Bow.Extra
{
    public class Target : MonoBehaviour, IArrowHittable
    {
        public float forceAmount = 1.0f;
        public Material otherMaterial;

        public void Hit(Arrow arrow)
        {
            ApplyMaterial();
            ApplyForce(arrow.transform.forward);
        }

        private void ApplyMaterial()
        {
            MeshRenderer meshRenderer = GetComponent<MeshRenderer>();
            meshRenderer.material = otherMaterial;
        }

        private void ApplyForce(Vector3 direction)
        {
            Rigidbody rBody = GetComponent<Rigidbody>();
            rBody.AddForce(direction * forceAmount);
        }
    }
}
