﻿namespace _PROJECT.Scripts.Bow.Extra
{
    public interface IArrowHittable
    {
        void Hit(Arrow arrow);
    }
}
