using FishNet.Object;
using UnityEngine;

namespace _PROJECT.Scripts.Bow
{
    public class StartTarget : NetworkBehaviour, IArrowHittable
    {
        public ArcheryRange archeryRange;
        public Canvas textCanvas;
        private MeshRenderer _meshRenderer;
        private BoxCollider _boxCollider;

        // Start is called before the first frame update
        void Start()
        {
            _meshRenderer = GetComponent<MeshRenderer>();
            _boxCollider = GetComponent<BoxCollider>();
        }
        
        [ObserversRpc]
        private void HideTarget()
        {
            _meshRenderer.enabled = false;
            _boxCollider.enabled = false;
            textCanvas.enabled = false;
        }

        [ObserversRpc]
        public void ShowTarget()
        {
            _meshRenderer.enabled = true;
            _boxCollider.enabled = true;
            textCanvas.enabled = true;
        }

        public void Hit(Arrow arrow)
        {
            if (!IsServer) return;
            if (arrow == null) return;

            Despawn(arrow.gameObject, DespawnType.Pool);
            HideTarget();
            archeryRange.StartRound();
        }
    }
}