using FishNet.Object;
using FishNet.Object.Synchronizing;
using TMPro;
using UnityEngine;

public class PointsText : NetworkBehaviour
{
    public TMP_Text text;
    [SyncVar]
    public float destroyTime = 2f;
    [SyncVar]
    public float upSpeed = 2f;

    [SyncVar]
    private float _destroyOnTime;

    private void Start()
    {
        if (!IsServer) return;
        _destroyOnTime = Time.time + destroyTime;
    }

    public void Update()
    {
        if (!IsServer) return;
        if (_destroyOnTime <= Time.time)
        {
            Despawn(gameObject, DespawnType.Pool);
        }

        float step = upSpeed * Time.deltaTime;
        var position = transform.position;
        transform.position = Vector3.MoveTowards(position,
            new Vector3(position.x, position.y + 1f, position.z), step);
    }

    [ObserversRpc]
    public void SetPoints(float points)
    {
        text.text = $"+{points}";
    }
}