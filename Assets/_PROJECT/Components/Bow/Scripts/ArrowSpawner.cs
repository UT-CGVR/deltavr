﻿using System;
using _PROJECT.Multiplayer;
using FishNet.Connection;
using FishNet.Object;
using FishNet.Object.Synchronizing;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace _PROJECT.Scripts.Bow
{
    public class ArrowSpawner : NetworkBehaviour
    {
        [SerializeField] private GameObject arrowPrefab;

        private XRMultiplayerInteractionManager _interactionManager;
        private Notch _notch;

        [SyncVar] private NetworkObject _arrowNetworkObject;

        public override void OnStartServer()
        {
            base.OnStartServer();
            _interactionManager = GameObject.Find("XR Interaction Manager").GetComponent<XRMultiplayerInteractionManager>();
            _notch = GetComponent<Notch>();
            CreateArrowServer();
        }

        public override void OnStartClient()
        {
            base.OnStartClient();
            _interactionManager = GameObject.Find("XR Interaction Manager").GetComponent<XRMultiplayerInteractionManager>();
            _notch = GetComponent<Notch>();
        }

        public void LaunchArrow()
        {
            Debug.Log("Sending arrow RPC at ");
            LaunchArrowRPC();
        }

        [ServerRpc(RequireOwnership = false)]
        private void LaunchArrowRPC()
        {
            if (!IsServer) return;
            Debug.Log("Got launch arrow RPC");
            if (_notch.hasSelection)
            {
                //OwnerDeselectRpc(Owner, _notch.firstInteractableSelected.transform.GetComponent<NetworkObject>());
                _interactionManager.SelectExit(_notch, _notch.firstInteractableSelected);
            }

            _arrowNetworkObject = null;

            Invoke(nameof(CreateArrowServer), _notch.recycleDelayTime + 0.1f);
        }

        private void CreateArrowServer()
        {
            if (!IsServer) return;
            Debug.Log("Spawning arrow at " + transform.position);
            GameObject arrowObject = Instantiate(arrowPrefab, transform.position, transform.rotation);
            Spawn(arrowObject, Owner);
            Arrow arrow = arrowObject.GetComponent<Arrow>();
            _arrowNetworkObject = arrow.GetComponent<NetworkObject>();
            _interactionManager.SelectEnter(_notch as IXRSelectInteractor, arrow);
            //OwnerSelectRpc(Owner, arrowObject.GetComponent<NetworkObject>());
        }
        
        public override void OnOwnershipClient(NetworkConnection prevOwner)
        {
            base.OnOwnershipClient(prevOwner);
            
            if (IsOwner && _arrowNetworkObject != null)
                TakeArrowOwnershipRPC();
        }
        
        [ServerRpc(RequireOwnership = false)]
        private void TakeArrowOwnershipRPC(NetworkConnection conn = null)
        {
            if (!IsServer) return;
            if (_arrowNetworkObject == null) return;
            _arrowNetworkObject.GiveOwnership(conn);
        }
    }
}