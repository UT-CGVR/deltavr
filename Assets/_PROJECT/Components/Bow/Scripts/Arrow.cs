﻿using System.Collections;
using _PROJECT.Multiplayer.NewBow;
using _PROJECT.Scripts.Bow;
using FishNet.Object;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class Arrow : XRGrabInteractable
{
    private ArrowCaster _caster;

    protected override void Awake()
    {
        base.Awake();
        _caster = GetComponent<ArrowCaster>();
    }

    protected override void OnSelectExited(SelectExitEventArgs args)
    {
        base.OnSelectExited(args);
        if (!_caster.IsServer) return;
        Debug.Log("Arrow select exit by " + args.interactorObject);

        if (args.interactorObject is not TwoHandedBowNotch notch)
        {
            Debug.Log("Arrow select exit not notch");
            return;
        }

        Debug.Log("Arrow select exit can release");
        _caster.LaunchArrow(notch);
    }
}
