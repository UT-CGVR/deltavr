using System;
using _PROJECT.Scripts.Bow;
using _PROJECT.Scripts.Bow.Extra;
using FishNet.Object;
using FishNet.Object.Synchronizing;
using UnityEngine;
using Random = UnityEngine.Random;

public class ArcheryTarget : NetworkBehaviour, IArrowHittable
{
    public GameObject pointsText;
    [SyncVar]
    public Vector3 endPosition;
    public float forwardSpeed = 2f;
    public Action<float> addScore;
    
    private bool _flipDirection;

    private void Awake()
    {
        _flipDirection = Random.value > 0.5f;
    }

    // Update is called once per frame
    void Update()
    {
        if (!IsServer) return;
        float step = forwardSpeed * Time.deltaTime;
        var position = transform.position;

        if (Math.Abs(position.x - endPosition.x) < 0.1) Destroy(gameObject);

        
        transform.position = Vector3.MoveTowards(position,
            new Vector3(endPosition.x, position.y, position.z), step);
    }

    public void Hit(Arrow arrow)
    {
        if (!IsServer) return;
        if (arrow == null) return;

        var position = transform.position;

        float score = (float)Math.Round(Vector3.Distance(position, endPosition));
        addScore(score);

        GameObject prefab = Instantiate(pointsText, position, Quaternion.Euler(0, 90f, 0), null);
        PointsText target = prefab.GetComponent<PointsText>();
        target.SetPoints(score);
        Spawn(prefab);

        Despawn(arrow.gameObject, DespawnType.Pool);
        Despawn(gameObject, DespawnType.Pool);
    }
}