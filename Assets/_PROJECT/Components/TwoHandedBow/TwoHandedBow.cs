using Audio;
using FishNet.Object;
using FishNet.Object.Synchronizing;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace _PROJECT.Multiplayer.NewBow
{
    public class TwoHandedBow : XRGrabInteractable
    {
        [SerializeField] private Transform pullMin;
        [SerializeField] private Transform pullMax;
        
        public AudioClipGroup pullSounds;
        public AudioClipGroup maxPullSounds;
        
        [SerializeField] private StringRenderer stringRenderer;
        
        private TwoHandedBowNotch _notch;
        
        public float PullAmount { get; set; }

        private new void Awake()
        {
            base.Awake();
            _notch = GetComponentInChildren<TwoHandedBowNotch>();
            if (_notch == null)
                Debug.LogError("Notch not found");
        }
        
        private void Update()
        {
            if (!isSelected) return;
            UpdatePull();
        }
        
        private void UpdatePull()
        {
            IXRInteractor secondInteractor = SecondInteractorSelecting();
            if (secondInteractor == null)
            {
                PullAmount = 0.0f;
                secondaryAttachTransform.position = pullMin.position;
                stringRenderer.UpdateString(secondaryAttachTransform.position);
                return;
            }

            Vector3 interactorPosition = secondInteractor.transform.position;
            
            float newPull = CalculatePull(interactorPosition);
            
            Vector3 pullPosition = Vector3.Lerp(pullMin.position, pullMax.position, newPull);
            secondaryAttachTransform.position = pullPosition;
            stringRenderer.UpdateString(secondaryAttachTransform.position);
            
            _notch.SetLaunchDirection(pullMin.position - pullMax.position);
            
            if (PullAmount == 0 & newPull > 0)
            {
                pullSounds.Play();
            }
            
            if (PullAmount < 0.98f & newPull >= 0.98f)
            {
                maxPullSounds.Play();
            }
            
            PullAmount = newPull;
        }

        private float CalculatePull(Vector3 pullPosition)
        {
            Vector3 pullDirection = pullPosition - pullMin.position;
            Vector3 targetDirection = pullMax.position - pullMin.position;

            float maxLength = targetDirection.magnitude;
            targetDirection.Normalize();

            float pullValue = Vector3.Dot(pullDirection, targetDirection) / maxLength;
            return Mathf.Clamp(pullValue, 0.0f, 1.0f);
        }

        private IXRInteractor SecondInteractorSelecting()
        {
            return interactorsSelecting.Count > 1 ? interactorsSelecting[1] : null;
        }
    }
}