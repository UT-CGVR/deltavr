using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit;

public class ActivateTeleportationRay : MonoBehaviour
{
    public GameObject teleportRay;
    public float rayHideDelay = 0.1f;

    public InputActionProperty activateTeleportRay;
    public TeleportationProvider teleportationProvider;

    // Update is called once per frame
    private void Start()
    {
        teleportRay.SetActive(false);
        activateTeleportRay.action.performed += ShowRay;
        activateTeleportRay.action.canceled += HideRayPerformed;
        teleportationProvider.endLocomotion += HideRayPerformed;
    }

    private void HideRayPerformed(InputAction.CallbackContext obj)
    {
        if (!teleportRay.activeSelf) return;
        if (IsInvoking(nameof(HideRay))) return;
        Invoke(nameof(HideRay), rayHideDelay);
    }

    private void HideRayPerformed(LocomotionSystem obj)
    {
        if (!teleportRay.activeSelf) return;
        if (IsInvoking(nameof(HideRay))) return;
        Invoke(nameof(HideRay), rayHideDelay);
    }

    private void ShowRay(InputAction.CallbackContext obj)
    {
        teleportRay.SetActive(true);
    }

    private void HideRay()
    {
        teleportRay.SetActive(false);
    }
}