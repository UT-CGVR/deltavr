using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class TeleportationProviderSetter : MonoBehaviour
{
    // hack script
    void Awake()
    {
        Debug.Log("Hack: Overwriting all teleport area providers in scene");
        TeleportationProvider provider = GetComponent<TeleportationProvider>();
        TeleportationArea[] teleportationAreas = FindObjectsOfType<TeleportationArea>();
        foreach (var area in teleportationAreas)
        {
            area.teleportationProvider = provider;
        }

        TeleportationAnchor[] teleportationAnchors = FindObjectsOfType<TeleportationAnchor>();
        foreach (var anchor in teleportationAnchors)
        {
            anchor.teleportationProvider = provider;
        }
    }
}