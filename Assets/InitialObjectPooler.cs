using System.Collections;
using System.Collections.Generic;
using FishNet.Object;
using UnityEngine;

public class InitialObjectPooler : NetworkBehaviour
{
    public List<GameObject> pooledPrefabs;
    
    public override void OnStartServer()
    {
        base.OnStartServer();
        
        foreach (var prefab in pooledPrefabs)
        {
            List<GameObject> pooledObjects = new List<GameObject>();
            for (int i = 0; i < 25; i++)
            {
                GameObject obj = Instantiate(prefab);
                Spawn(obj);
            }

            foreach (var obj in pooledObjects)
            {
                Despawn(obj, DespawnType.Pool);
            }
        }
    }
}
