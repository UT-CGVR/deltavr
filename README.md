# DeltaVR

DeltaVR is a virtual reality experience set in the Delta Centre of the University of Tartu. It was designed and implemented in a over three theses. The proiect used the Delta Building Visualization project as a basis for the building and built upon it, adding missing
details and improving the performance. DeltaVR has multiplayer support, which allows players to explore the building together in PCVR, Quest 2 and non-VR versions.

## Gameplay Sample Footage (DeltaVR 2021)

https://youtu.be/AoRN4eluiWY

## History

2023 version:

https://comserv.cs.ut.ee/ati_thesis/datasheet.php?id=77065&language=en 

(See Extras for build)

2022 version: 

https://comserv.cs.ut.ee/ati_thesis/datasheet.php?id=74390

https://gitlab.com/Joonasp1/deltavr-multiplayer-builds

2021 version: 

https://comserv.cs.ut.ee/ati_thesis/datasheet.php?id=71682

https://drive.google.com/file/d/1n19_Wa69vCX6s6zKYoSYKirpHcfJHqaM/view?usp=sharing

